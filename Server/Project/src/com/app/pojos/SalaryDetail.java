package com.app.pojos;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
@Entity
@Table(name="SALARY_DETAIL")

public class SalaryDetail {

	private String panId;
	private Double basic;
	private Double hra;
	private Double specialAllowance;
	private Double lta;
	private BasicDetails user;
	public SalaryDetail() {
		// TODO Auto-generated constructor stub
	}
	@Id
	@Column(name="PAN_ID")
	public String getPanId() {
		return panId;
	}
	public void setPanId(String panId) {
		this.panId = panId;
	}
	@Column(name="BASIC")
	public Double getBasic() {
		return basic;
	}
	public void setBasic(Double basic) {
		this.basic = basic;
	}
	@Column(name="HRA")
	public Double getHra() {
		return hra;
	}
	public void setHra(Double hra) {
		this.hra = hra;
	}
	@Column(name="SPECIAL_ALLOWANCE")
	public Double getSpecialAllowance() {
		return specialAllowance;
	}
	public void setSpecialAllowance(Double specialAllowance) {
		this.specialAllowance = specialAllowance;
	}
	@Column(name="LTA")
	public Double getLta() {
		return lta;
	}
	public void setLta(Double lta) {
		this.lta = lta;
	}
	@OneToOne
	@JoinColumn(name="PAN_ID")
	@JsonBackReference
	public BasicDetails getUser() {
		return user;
	}
	public void setUser(BasicDetails user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "SalaryDetail [panId=" + panId + ", basic=" + basic + ", hra=" + hra + ", specialAllowance="
				+ specialAllowance + ", lta=" + lta + "]";
	}
	
}
