package com.app.pojos;

import javax.persistence.*;

@Entity
@Table(name="STATES")
public class States {
	private Long pinCode;
	private String states;
	private String city;
	
	public States() {
		// TODO Auto-generated constructor stub
	}
	@Id
	@Column(name="PINCODE")
	public Long getPinCode() {
		return pinCode;
	}

	public void setPinCode(Long pinCode) {
		this.pinCode = pinCode;
	}
	@Column(name="STATES")
	public String getStates() {
		return states;
	}

	public void setStates(String states) {
		this.states = states;
	}
	@Column(name="CITY")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "States [pinCode=" + pinCode + ", states=" + states + ", city=" + city + "]";
	}
	
}
