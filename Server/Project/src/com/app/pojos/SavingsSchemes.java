package com.app.pojos;
import javax.persistence.*;

@Entity
@Table(name="SavingSchemes")
public class SavingsSchemes {

	private Integer id;
	private String name;
	private String description;
	private String category;
	
	
	public SavingsSchemes() {
		// TODO Auto-generated constructor stub
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	@Column(length=10000)
	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	@Override
	public String toString() {
		return "SavingsSchemes [Id=" + id + ", name=" + name + ", description=" + description + ", category=" + category
				+ "]";
	}
	
	
	
}
