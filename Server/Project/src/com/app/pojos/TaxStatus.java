package com.app.pojos;
import javax.persistence.*;
@Entity
@Table(name="TaxStatus")
public class TaxStatus {

	
	private String panId;
	private String firstName;
	private double taxableAmount;
	private double taxAmount;
	private String status;
	private String state;
	
	

	public TaxStatus() {
		// TODO Auto-generated constructor stub
	}
	
	
	@Column(name="state")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	@Id
	@Column(name="pan_id")
	public String getPanId() {
		return panId;
	}
	public void setPanId(String panId) {
		this.panId = panId;
	}
	@Column(name="first_name")
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@Column(name="taxable_amount")
	public double getTaxableAmount() {
		return taxableAmount;
	}
	public void setTaxableAmount(double taxableAmount) {
		this.taxableAmount = taxableAmount;
	}
	@Column(name="tax_amount")
	public double getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}
	@Column(name="status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "TaxStatus [panId=" + panId + ", firstName=" + firstName + ", taxableAmount=" + taxableAmount
				+ ", taxAmount=" + taxAmount + "]";
	}
	
}
