package com.app.pojos;

import java.util.Date;
import javax.persistence.*;



import com.fasterxml.jackson.annotation.JsonFormat;

import com.fasterxml.jackson.annotation.JsonManagedReference;
@Entity
@Table(name="BASIC_DETAIL")
public class BasicDetails {

	private String panId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String contactNo;
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dob;
	private String email;
	private String password;
	private SalaryDetail salaryDetail;
	private AdditionalIncome additionalIncome;
	private Address address;
	private Deduction80D deduction80D;
	private Deduction80C deduction80C;
	public BasicDetails() {
		// TODO Auto-generated constructor stub
	}
	@Id
	@Column(name="PAN_ID")
	//@JsonIgnore
	public String getPanId() {
		return panId;
	}

	public void setPanId(String panId) {
		this.panId = panId;
	}
	@Column(name="FIRST_NAME",nullable=false)
	//@JsonIgnore
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@Column(name="MIDDLE_NAME",nullable=false)
	//@JsonIgnore
	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	@Column(name="LAST_NAME",nullable=false)
	//@JsonIgnore
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@Column(name="CONTACT_NO")
	//@JsonIgnore
	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="DOB",nullable=false)
	//@JsonIgnore
	public Date getDob() {
		return dob;
	}
	
	public void setDob(Date dob) {
		this.dob = dob;
	}
	@Column(name="EMAIL_ID",nullable=false)
	//@JsonIgnore
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	@Column(name="PASSWORD",nullable=false)
	//@JsonIgnore
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@OneToOne(mappedBy="user",cascade=CascadeType.ALL)
	@JsonManagedReference
	public SalaryDetail getSalaryDetail() {
		return salaryDetail;
	}
	public void setSalaryDetail(SalaryDetail salaryDetail) {
		this.salaryDetail = salaryDetail;
	}
	@OneToOne(mappedBy="user",cascade=CascadeType.ALL)
	@JsonManagedReference
	public AdditionalIncome getAdditionalIncome() {
		return additionalIncome;
	}
	public void setAdditionalIncome(AdditionalIncome additionalIncome) {
		this.additionalIncome = additionalIncome;
	}
	@OneToOne(mappedBy="user",cascade=CascadeType.ALL)
	@JsonManagedReference
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	@OneToOne(mappedBy="user",cascade=CascadeType.ALL)
	@JsonManagedReference
	public Deduction80D getDeduction80D() {
		return deduction80D;
	}
	public void setDeduction80D(Deduction80D deduction80d) {
		deduction80D = deduction80d;
	}
	@OneToOne(mappedBy="user",cascade=CascadeType.ALL)
	@JsonManagedReference
	public Deduction80C getDeduction80C() {
		return deduction80C;
	}
	public void setDeduction80C(Deduction80C deduction80c) {
		deduction80C = deduction80c;
	}
	public void addSalary(SalaryDetail s)
	{
		this.setSalaryDetail(s);
		s.setUser(this);
	}
	public void removeSalary(SalaryDetail s)
	{
		this.setSalaryDetail(null);
		s.setUser(null);
	}
	public void addAdditionalIncome(AdditionalIncome a)
	{
		this.setAdditionalIncome(a);
		a.setUser(this);
	}
	public void removeAdditionalIncome(AdditionalIncome a)
	{
		this.setAdditionalIncome(null);
		a.setUser(null);
	}
	public void addAddress(Address a)
	{
		this.setAddress(a);
		a.setUser(this);
	}
	public void removeAddress(Address a)
	{
		this.setAddress(null);
		a.setUser(null);
	}
	public void addDeduction80D(Deduction80D d)
	{
		this.setDeduction80D(d);
		d.setUser(this);
	}
	public void removeDeduction80D(Deduction80D d)
	{
		this.setDeduction80D(null);
		d.setUser(null);
	}
	public void addDeduction80C(Deduction80C d)
	{
		this.setDeduction80C(d);
		d.setUser(this);
	}
	public void removeDeduction80C(Deduction80C d)
	{
		this.setDeduction80C(null);
		d.setUser(null);
	}
	
	@Override
	public String toString() {
		return "BasicDetails [panId=" + panId + ", firstName=" + firstName + ", middleName=" + middleName
				+ ", lastName=" + lastName + ", contactNo=" + contactNo + ", dob=" + dob + ", email=" + email
				+ ", password=" + password + "]";
	}
	
	
}
