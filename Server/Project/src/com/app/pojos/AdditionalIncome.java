package com.app.pojos;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="ADDITIONAL_INCOME")
public class AdditionalIncome {

	private String panId;
	private Double houseRent;
	private Double interestLoan;
	private Double mutualFunds;
	private Double iSavingAcc;
	private Double iBonds;
	private Double otherIncome;
	private BasicDetails user;
	public AdditionalIncome() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@Column(name="PAN_ID")
	public String getPanId() {
		return panId;
	}

	public void setPanId(String panId) {
		this.panId = panId;
	}
	@Column(name="HOUSE_RENT")
	public Double getHouseRent() {
		return houseRent;
	}

	public void setHouseRent(Double houseRent) {
		this.houseRent = houseRent;
	}
	@Column(name="INTEREST_LOAN")
	public Double getInterestLoan() {
		return interestLoan;
	}

	public void setInterestLoan(Double interestLoan) {
		this.interestLoan = interestLoan;
	}
	@Column(name="MUTUAL_FUNDS")
	public Double getMutualFunds() {
		return mutualFunds;
	}

	public void setMutualFunds(Double mutualFunds) {
		this.mutualFunds = mutualFunds;
	}
	@Column(name="INTEREST_SAVINGS_ACCOUNT")
	public Double getiSavingAcc() {
		return iSavingAcc;
	}

	public void setiSavingAcc(Double iSavingAcc) {
		this.iSavingAcc = iSavingAcc;
	}
	@Column(name="INTEREST_BONDS")
	public Double getiBonds() {
		return iBonds;
	}

	public void setiBonds(Double iBonds) {
		this.iBonds = iBonds;
	}
	@Column(name="ANY_OTHER_INCOME")
	public Double getOtherIncome() {
		return otherIncome;
	}

	public void setOtherIncome(Double otherIncome) {
		this.otherIncome = otherIncome;
	}
	@OneToOne
	@JoinColumn(name="PAN_ID")
	@JsonBackReference
	public BasicDetails getUser() {
		return user;
	}

	public void setUser(BasicDetails user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "AdditionalIncome [panId=" + panId + ", houseRent=" + houseRent + ", interestLoan=" + interestLoan
				+ ", mutualFunds=" + mutualFunds + ", iSavingAcc=" + iSavingAcc + ", iBonds=" + iBonds
				+ ", otherIncome=" + otherIncome + "]";
	}
	
}
