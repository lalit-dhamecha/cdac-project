package com.app.pojos;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
@Table(name="DEDUCTION_80C")

public class Deduction80C {
	private String panId;
	private Double epf;
	private Double ppf;
	private Double scss;
	private Double nsc;
	private Double taxSavingFD;
	private Double taxSavingBonds;
	private Double taxSavingFunds;
	private Double lic;
	private Double nps;
	private Double pensionPlan;
	private Double housingLoan;
	private Double sukanyaSamriddhi;
	private Double tutionFees;
	private BasicDetails user;
	public Deduction80C() {
		// TODO Auto-generated constructor stub
	}
	@Id
	@Column(name="PAN_ID")
	public String getPanId() {
		return panId;
	}

	public void setPanId(String panId) {
		this.panId = panId;
	}
	@Column(name="EPF")
	public Double getEpf() {
		return epf;
	}

	public void setEpf(Double epf) {
		this.epf = epf;
	}
	@Column(name="PPF")
	public Double getPpf() {
		return ppf;
	}

	public void setPpf(Double ppf) {
		this.ppf = ppf;
	}
	@Column(name="SCSS")
	public Double getScss() {
		return scss;
	}

	public void setScss(Double scss) {
		this.scss = scss;
	}
	@Column(name="NSC")
	public Double getNsc() {
		return nsc;
	}

	public void setNsc(Double nsc) {
		this.nsc = nsc;
	}
	@Column(name="TAX_SAVING_FD")
	public Double getTaxSavingFD() {
		return taxSavingFD;
	}

	public void setTaxSavingFD(Double taxSavingFD) {
		this.taxSavingFD = taxSavingFD;
	}
	@Column(name="TAX_SAVING_BONDS")
	public Double getTaxSavingBonds() {
		return taxSavingBonds;
	}

	public void setTaxSavingBonds(Double taxSavingBonds) {
		this.taxSavingBonds = taxSavingBonds;
	}
	@Column(name="TAX_SAVING_MUTUAL_FUNDS")
	public Double getTaxSavingFunds() {
		return taxSavingFunds;
	}

	public void setTaxSavingFunds(Double taxSavingFunds) {
		this.taxSavingFunds = taxSavingFunds;
	}
	@Column(name="LIC")
	public Double getLic() {
		return lic;
	}

	public void setLic(Double lic) {
		this.lic = lic;
	}
	@Column(name="NPS")
	public Double getNps() {
		return nps;
	}

	public void setNps(Double nps) {
		this.nps = nps;
	}
	@Column(name="PENSION_PLANS")
	public Double getPensionPlan() {
		return pensionPlan;
	}

	public void setPensionPlan(Double pensionPlan) {
		this.pensionPlan = pensionPlan;
	}
	@Column(name="HOUSING_LOAN")
	public Double getHousingLoan() {
		return housingLoan;
	}

	public void setHousingLoan(Double housingLoan) {
		this.housingLoan = housingLoan;
	}
	@Column(name="SUKANKYA_SAMRIDDHI")
	public Double getSukanyaSamriddhi() {
		return sukanyaSamriddhi;
	}

	public void setSukanyaSamriddhi(Double sukanyaSamriddhi) {
		this.sukanyaSamriddhi = sukanyaSamriddhi;
	}
	@Column(name="TUTION_FEES")
	public Double getTutionFees() {
		return tutionFees;
	}

	public void setTutionFees(Double tutionFees) {
		this.tutionFees = tutionFees;
	}
	@OneToOne
	@JoinColumn(name="PAN_ID")
	@JsonBackReference
	public BasicDetails getUser() {
		return user;
	}
	public void setUser(BasicDetails user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Deduction80C [panId=" + panId + ", epf=" + epf + ", ppf=" + ppf + ", scss=" + scss + ", nsc=" + nsc
				+ ", taxSavingFD=" + taxSavingFD + ", taxSavingBonds=" + taxSavingBonds + ", taxSavingFunds="
				+ taxSavingFunds + ", lic=" + lic + ", nps=" + nps + ", pensionPlan=" + pensionPlan + ", housingLoan="
				+ housingLoan + ", sukanyaSamriddhi=" + sukanyaSamriddhi + ", tutionFees=" + tutionFees + "]";
	}
	
}
