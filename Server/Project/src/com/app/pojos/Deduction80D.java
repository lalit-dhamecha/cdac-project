package com.app.pojos;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
@Entity
@Table(name="DEDUCTION_80D")
public class Deduction80D {

	private String panId;
	private Double medicalIns;
	private Double iEducationalLoan;
	private Double nhra;
	private Double physicallyDisable;
	private Double seniorCitizen;
	private BasicDetails user;
	public Deduction80D() {
		// TODO Auto-generated constructor stub
	}
	@Id
	@Column(name="PAN_ID")
	public String getPanId() {
		return panId;
	}
	public void setPanId(String panId) {
		this.panId = panId;
	}
	@Column(name="MEDICAL_INS")
	public Double getMedicalIns() {
		return medicalIns;
	}
	public void setMedicalIns(Double medicalIns) {
		this.medicalIns = medicalIns;
	}
	@Column(name="INT_EDUCATION_LOAN")
	public Double getiEducationalLoan() {
		return iEducationalLoan;
	}
	public void setiEducationalLoan(Double iEducationalLoan) {
		this.iEducationalLoan = iEducationalLoan;
	}
	@Column(name="NO_HRA")
	public Double getNhra() {
		return nhra;
	}
	public void setNhra(Double nhra) {
		this.nhra = nhra;
	}
	@Column(name="PHYSICALLY_DISABLE")
	public Double getPhysicallyDisable() {
		return physicallyDisable;
	}
	public void setPhysicallyDisable(Double physicallyDisable) {
		this.physicallyDisable = physicallyDisable;
	}
	@Column(name="SENIOR_CITIZENS")
	public Double getSeniorCitizen() {
		return seniorCitizen;
	}
	public void setSeniorCitizen(Double seniorCitizen) {
		this.seniorCitizen = seniorCitizen;
	}
	@OneToOne
	@JoinColumn(name="PAN_ID")
	@JsonBackReference
	public BasicDetails getUser() {
		return user;
	}
	public void setUser(BasicDetails user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Deduction80D [panId=" + panId + ", medicalIns=" + medicalIns + ", iEducationalLoan=" + iEducationalLoan
				+ ", nhra=" + nhra + ", physicallyDisable=" + physicallyDisable + ", seniorCitizen=" + seniorCitizen
				+ "]";
	}
	
}
