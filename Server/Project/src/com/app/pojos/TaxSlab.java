package com.app.pojos;
import javax.persistence.*;
@Entity
@Table(name="Tax_Slab")
public class TaxSlab {

	private Integer id;
	private Long maxLimit;
	private Long minLimit;
	private int roi;
	
	public TaxSlab() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="max_limit")
	public Long getMaxLimit() {
		return maxLimit;
	}

	public void setMaxLimit(Long maxLimit) {
		this.maxLimit = maxLimit;
	}

	@Column(name="min_limit")
	public Long getMinLimit() {
		return minLimit;
	}

	public void setMinLimit(Long minLimit) {
		this.minLimit = minLimit;
	}

	@Column(name="roi")
	public int getRoi() {
		return roi;
	}

	public void setRoi(int roi) {
		this.roi = roi;
	}

	@Override
	public String toString() {
		return "TaxSlab [id=" + id + ", maxLimit=" + maxLimit + ", minLimit=" + minLimit + ", roi=" + roi + "]";
	}
	
}
