package com.app.pojos;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="ADDRESS")
public class Address {

	private String panId;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private Long pincode;
	private BasicDetails user;
	public Address() {
		// TODO Auto-generated constructor stub
	}
	@Id
	@Column(name="PAN_ID")
	public String getPanId() {
		return panId;
	}
	public void setPanId(String panId) {
		this.panId = panId;
	}
	@Column(name="ADDRESS_1")
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	@Column(name="ADDRESS_2")
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	@Column(name="CITY")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Column(name="STATE")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Column(name="PINCODE")
	public Long getPincode() {
		return pincode;
	}
	public void setPincode(Long pincode) {
		this.pincode = pincode;
	}
	@OneToOne
	@JoinColumn(name="PAN_ID")
	@JsonBackReference
	public BasicDetails getUser() {
		return user;
	}
	public void setUser(BasicDetails user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Address [panId=" + panId + ", address1=" + address1 + ", address2=" + address2 + ", city=" + city
				+ ", state=" + state + ", pincode=" + pincode + "]";
	}
	
}
