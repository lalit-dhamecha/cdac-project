package com.app.dao;

import java.util.List;

import com.app.pojos.SavingsSchemes;
import com.app.pojos.TaxSlab;
import com.app.pojos.TaxStatus;

public interface IAdminDao {

	String registerScheme(SavingsSchemes s);
	List<TaxSlab> getTaxSlab();
	TaxSlab getSlab(int id);
	String updateSlab(TaxSlab t);
	List<SavingsSchemes> getSchemes();
	List<TaxStatus> getTaxStatus();
	String updateTaxStatus(String panId,String status);
	String getEmail(String panId);
	List<TaxStatus> getTaxReport();
	List<TaxStatus> getStatewiseReport(String state);
	List<String> getStates();
}
