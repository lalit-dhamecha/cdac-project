package com.app.dao;

import java.util.List;

import com.app.pojos.AdditionalIncome;
import com.app.pojos.Address;
import com.app.pojos.BasicDetails;
import com.app.pojos.Deduction80C;
import com.app.pojos.Deduction80D;
import com.app.pojos.SalaryDetail;
import com.app.pojos.TaxSlab;
import com.app.pojos.TaxStatus;

public interface IUserDao {
	List<BasicDetails> getDetails();
	String registerUser(BasicDetails b);
	BasicDetails validateUser(String email,String password)throws RuntimeException;
	String updateAddress(Address a);
	String updateSalaryDetails(SalaryDetail s);
	String updateAdditionalIncome(AdditionalIncome a);
	String updateDeduction80C(Deduction80C d);
	String updateDeduction80D(Deduction80D d);
	double getSalleryDetails(String panId);
	double getAdditionalIncome(String panId);
	double getDeduction80C(String panId);
	double getDeduction80D(String panId);
	List<TaxSlab> getTaxSlab();
	String updateTaxStatus(TaxStatus tax);
	TaxStatus getTaxStatus(String panId);
	String deleteTaxStatus(String panId);
	SalaryDetail getForm1(String panId);
	AdditionalIncome getForm2(String panId);
	Deduction80C getForm3(String panId);
	Deduction80D getForm4(String panId);
	Address getAddress(String panId);
}
