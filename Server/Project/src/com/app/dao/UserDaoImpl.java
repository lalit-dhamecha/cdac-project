package com.app.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.pojos.AdditionalIncome;
import com.app.pojos.Address;
import com.app.pojos.BasicDetails;
import com.app.pojos.Deduction80C;
import com.app.pojos.Deduction80D;
import com.app.pojos.SalaryDetail;
import com.app.pojos.TaxSlab;
import com.app.pojos.TaxStatus;

@Repository
@Transactional
public class UserDaoImpl implements IUserDao {

	@Autowired
	private SessionFactory sf;
	@Override
	public List<BasicDetails> getDetails() {
		String jpql="select b from BasicDetails b";
		return sf.getCurrentSession().createQuery(jpql,BasicDetails.class).getResultList();
	}
	@Override
	public String registerUser(BasicDetails b) {
		sf.getCurrentSession().persist(b);
		return "User registered successfully with id  "+b.getPanId() ;
	}
	@Override
	public BasicDetails validateUser(String email, String password)throws RuntimeException {
		String jpql="select b from BasicDetails b where b.email=:em and b.password=:pass";
		BasicDetails b= sf.getCurrentSession().createQuery(jpql,BasicDetails.class).setParameter("em", email).setParameter("pass", password).getSingleResult();
		return b;
	}
	@Override
	public String updateAddress(Address a) {
		 sf.getCurrentSession().update(a);
		 return "update successfull";
	}
	@Override
	public String updateSalaryDetails(SalaryDetail s) {
		 sf.getCurrentSession().update(s);
		return "update successfull";
	}
	@Override
	public String updateAdditionalIncome(AdditionalIncome a) {
		sf.getCurrentSession().update(a);
		return "update successfull";
	}
	@Override
	public String updateDeduction80C(Deduction80C d) {
		sf.getCurrentSession().update(d);
		return "update successfull";
	}
	@Override
	public String updateDeduction80D(Deduction80D d) {
		sf.getCurrentSession().update(d);
		return "update successfull";
	}
	@Override
	public double getSalleryDetails(String panId) {
		String jpql="select b.basic + b.hra + b.specialAllowance + b.lta from SalaryDetail b where b.panId=:panId";
		return sf.getCurrentSession().createQuery(jpql, Double.class).setParameter("panId", panId).getSingleResult();
	}
	@Override
	public double getAdditionalIncome(String panId) {
		String jpql="select IFNULL(b.houseRent,0) + IFNULL(b.interestLoan,0) + IFNULL(b.mutualFunds,0) + IFNULL(b.iSavingAcc,0) + IFNULL(b.iBonds,0) + IFNULL(b.otherIncome,0) from AdditionalIncome b where b.panId=:panId";
		return sf.getCurrentSession().createQuery(jpql, Double.class).setParameter("panId", panId).getSingleResult();
	}
	@Override
	public double getDeduction80C(String panId) {
		String jpql="select IFNULL(b.epf,0) + IFNULL(b.ppf,0) + IFNULL(b.scss,0) + IFNULL(b.nsc,0) + IFNULL(b.taxSavingFD,0) + IFNULL(b.taxSavingBonds,0) + IFNULL(b.taxSavingFunds,0) + "
				+ "IFNULL(b.lic,0) + IFNULL(b.nps,0) + IFNULL(b.pensionPlan,0) + IFNULL(b.housingLoan,0) + IFNULL(b.sukanyaSamriddhi,0) + IFNULL(b.tutionFees,0) from Deduction80C b where b.panId=:panId";
		return sf.getCurrentSession().createQuery(jpql, Double.class).setParameter("panId", panId).getSingleResult();
	}
	@Override
	public double getDeduction80D(String panId) {
		String jpql="select IFNULL(b.medicalIns,0) + IFNULL(b.iEducationalLoan,0) + IFNULL(b.nhra,0) + IFNULL(b.physicallyDisable,0) + IFNULL(b.seniorCitizen,0) from Deduction80D b where b.panId=:panId";
		return sf.getCurrentSession().createQuery(jpql, Double.class).setParameter("panId", panId).getSingleResult();
	}
	@Override
	public List<TaxSlab> getTaxSlab() {
		String jpql="select t from TaxSlab t";
		return sf.getCurrentSession().createQuery(jpql, TaxSlab.class).getResultList();
	}
	@Override
	public String updateTaxStatus(TaxStatus tax) {
			sf.getCurrentSession().merge(tax);	
		return "update successfull";
	}
	@Override
	public TaxStatus getTaxStatus(String panId) {
		
		return sf.getCurrentSession().get(TaxStatus.class, panId);
	}
	@Override
	public String deleteTaxStatus(String panId) {
		TaxStatus tax=sf.getCurrentSession().get(TaxStatus.class, panId);
		sf.getCurrentSession().remove(tax);
		return "delete successfull";
	}
	@Override
	public SalaryDetail getForm1(String panId) {
		
		return sf.getCurrentSession().get(SalaryDetail.class, panId);
	}
	@Override
	public AdditionalIncome getForm2(String panId) {
		
		return sf.getCurrentSession().get(AdditionalIncome.class, panId);
	}
	@Override
	public Deduction80C getForm3(String panId) {
		
		return sf.getCurrentSession().get(Deduction80C.class, panId);
	}
	@Override
	public Deduction80D getForm4(String panId) {
		
		return sf.getCurrentSession().get(Deduction80D.class, panId);
	}
	@Override
	public Address getAddress(String panId) {
		
		return sf.getCurrentSession().get(Address.class, panId);
	}
	

	
	
}
