package com.app.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.pojos.SavingsSchemes;
import com.app.pojos.TaxSlab;
import com.app.pojos.TaxStatus;

@Repository
@Transactional
public class AdminDao implements IAdminDao {

	@Autowired
	private SessionFactory sf;

	@Override
	public String registerScheme(SavingsSchemes s) {
		sf.getCurrentSession().persist(s);
		return "Scheme added successfully";
		
	}

	@Override
	public List<TaxSlab> getTaxSlab() {
			String jpql="select t from TaxSlab t";
		return sf.getCurrentSession().createQuery(jpql, TaxSlab.class).getResultList();
	}


	@Override
	public TaxSlab getSlab(int id) {
		
		return sf.getCurrentSession().get(TaxSlab.class, id);
	}

	@Override
	public String updateSlab(TaxSlab t) {
		sf.getCurrentSession().update(t);
		return "update successfull";
	}

	@Override
	public List<SavingsSchemes> getSchemes() {
		String jpql="select s from SavingsSchemes s";
		return sf.getCurrentSession().createQuery(jpql,SavingsSchemes.class).getResultList();
	}

	@Override
	public List<TaxStatus> getTaxStatus() {
		String jpql="select s from TaxStatus s";
		return sf.getCurrentSession().createQuery(jpql,TaxStatus.class).getResultList();
	}

	@Override
	public String updateTaxStatus(String panId, String status) {
		String jpql="update TaxStatus t set t.status=:status where t.panId=:panId";
	   sf.getCurrentSession().createQuery(jpql).setParameter("status", status).setParameter("panId", panId).executeUpdate();
	   return "update successfull";
	}

	@Override
	public String getEmail(String panId) {
		String jpql="select b.email from BasicDetails b where b.panId=:panId";
		
		return sf.getCurrentSession().createQuery(jpql, String.class).setParameter("panId", panId).getSingleResult();
	}

	@Override
	public List<TaxStatus> getTaxReport() {
		String status="Approved";
		String jpql="select s from TaxStatus s where s.status=:status";
		List<TaxStatus> tax=sf.getCurrentSession().createQuery(jpql,TaxStatus.class).setParameter("status",status ).getResultList();
		System.out.println(tax);
		return tax;
	}

	@Override
	public List<TaxStatus> getStatewiseReport(String state) {
		String jpql="select s from TaxStatus s where s.state=:state";
		List<TaxStatus> list=sf.getCurrentSession().createQuery(jpql, TaxStatus.class).setParameter("state",state).getResultList();
		System.out.println(list);
		return list;
	}

	@Override
	public List<String> getStates() {
		String jpql="select distinct s.state from TaxStatus s";
		return sf.getCurrentSession().createQuery(jpql, String.class).getResultList();
	}

	
}
