package com.app.controller;



import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.app.dao.IAdminDao;
import com.app.pojos.Credentials;
import com.app.pojos.SavingsSchemes;
import com.app.pojos.TaxSlab;
import com.app.pojos.TaxStatus;
@CrossOrigin
@RestController
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private  IAdminDao dao;
	@Autowired
	private JavaMailSender sender;
	public AdminController() {
		System.out.println("inside admin controller");
	}
	@PostMapping("/login")
	public ResponseEntity<String> loginAdmin(@RequestBody Credentials c)
	{
		String email=c.getEmail();
		String password=c.getPassword();
		System.out.println("email: "+email+"  ,"+ "password:  "+password);
				if(email.equals("lalitd@gmail.com") && password.equals("Lalit@1234"))
			return new ResponseEntity<>("login successfull",HttpStatus.OK);
			else
			return new ResponseEntity<>("user login failed ", HttpStatus.OK);
		
	}
	@PostMapping("/addScheme")
	public ResponseEntity<String> registerScheme(@RequestBody SavingsSchemes s)
	{
		System.out.println("rest srvr :  Scheme " + s);
		try {
			return new ResponseEntity<>(dao.registerScheme(s), HttpStatus.CREATED);

		} catch (RuntimeException e) {
			System.out.println("err in reg " + e);
			return new ResponseEntity<>("Scheme add failed : " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/taxSlab")
	public ResponseEntity<?> getTaxSlab(){
		System.out.println("inside get tax slab");
		return new ResponseEntity<>(dao.getTaxSlab(),HttpStatus.OK);
	}
	@GetMapping("/schemes")
	public ResponseEntity<?> getSchemes(){
		System.out.println("inside get schemes");
		return new ResponseEntity<>(dao.getSchemes(),HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> getSlab(@PathVariable int id)
	{
		System.out.println("id"+id);
		return new ResponseEntity<>(dao.getSlab(id),HttpStatus.OK);
	}
	@PutMapping("/updateSlab")
	public ResponseEntity<?> updateSlab(@RequestBody TaxSlab t)
	{
		System.out.println(t);
		return new ResponseEntity<>(dao.updateSlab(t),HttpStatus.OK);
	}
	@GetMapping("/report")
	public ResponseEntity<?> getReport(){
		System.out.println("inside get report");
		return new ResponseEntity<>(dao.getTaxStatus(),HttpStatus.OK);
	}
	@GetMapping("/revenue")
	public ResponseEntity<?> getRevenue(){
		System.out.println("inside get revenue");
		System.out.println();
		return new ResponseEntity<>(dao.getTaxReport(),HttpStatus.OK);
	}
	@GetMapping("/state-report/{state}")
	public ResponseEntity<?> getStateReport(@PathVariable String state){
		System.out.println("inside get state report");
		System.out.println(state);
		return new ResponseEntity<>(dao.getStatewiseReport(state),HttpStatus.OK);
	}
	@GetMapping("/states")
	public ResponseEntity<?> getStates(){
		System.out.println("inside get state");
		
		return new ResponseEntity<>(dao.getStates(),HttpStatus.OK);
	}
	@PostMapping("/changeStatus")
	public ResponseEntity<?> updateStatus(@RequestBody TaxStatus t)
	{
		System.out.println("panid"+ t.getPanId()+ "  status: "+t.getStatus());
		this.sendMail(t.getPanId(), t.getStatus());
		return new ResponseEntity<String>(dao.updateTaxStatus(t.getPanId(), t.getStatus()),HttpStatus.OK);
	}
	
	//@GetMapping("/email/{email}")
	public ResponseEntity<String> sendMail(String panId,String status)
	{
		try {
		String email=dao.getEmail(panId);
		System.out.println(email);
		SimpleMailMessage mesg=new SimpleMailMessage();
		mesg.setTo(email);
		mesg.setSubject("Regarding Approval");
		mesg.setText("Your ITR application Status is :"+status);
		sender.send(mesg);
		return new ResponseEntity<String>("sent mail",HttpStatus.OK);
		}catch (RuntimeException e) {
			return new ResponseEntity<String>("Error while sending"+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
