package com.app.controller;

import java.util.List;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


import org.springframework.web.bind.annotation.RestController;

import com.app.dao.IUserDao;
import com.app.pojos.AdditionalIncome;
import com.app.pojos.Address;
import com.app.pojos.BasicDetails;
import com.app.pojos.Deduction80C;
import com.app.pojos.Deduction80D;
import com.app.pojos.SalaryDetail;
import com.app.pojos.TaxSlab;
import com.app.pojos.TaxStatus;

@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {

	
	@Autowired
	private IUserDao dao;
	private BasicDetails user=new BasicDetails();
	public UserController() {
	System.out.println("inside user controller");
	}
	
	@GetMapping
	public List<BasicDetails> fetchAllUsers(){
		System.out.println("fetch all");
		List<BasicDetails> l=dao.getDetails();
		for (BasicDetails basicDetails : l) {
			System.out.println("List="+basicDetails);
			System.out.println(basicDetails.getAdditionalIncome());
		}
		return l;
	}
	
	@PostMapping("/register")
	public ResponseEntity<String> registerUser(@RequestBody BasicDetails b)
	{
		System.out.println("rest srvr : reg user " + b);
		try {
			return new ResponseEntity<>(dao.registerUser(b), HttpStatus.CREATED);

		} catch (RuntimeException e) {
			System.out.println("err in reg " + e);
			return new ResponseEntity<>("user reg failed : " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/login")
	public ResponseEntity<String> loginUser(@RequestBody BasicDetails b,HttpSession hs)
	{
		System.out.println("email: "+b.getEmail()+"  ,"+ "password:  "+b.getPassword());
		try {
			BasicDetails b1=dao.validateUser(b.getEmail(), b.getPassword());
			user=b1;
			System.out.println(user);
			//hs.setAttribute("user", b1);
			if(user.getAddress().getState()==null)
				return new ResponseEntity<>("login successfull go to address",HttpStatus.OK);
			else
			return new ResponseEntity<>("login successfull",HttpStatus.OK);
		}catch (RuntimeException e) {
			System.out.println("err in login " + e);
			return new ResponseEntity<>("user login failed : " + e.getMessage(), HttpStatus.OK);
		}
		
	}
	
	@PutMapping("/address")
	public ResponseEntity<String> updateAddress(@RequestBody Address a,HttpSession hs)
	{
		System.out.println("inside put");
		System.out.println(a);
		//BasicDetails user=(BasicDetails)hs.getAttribute("user");
		System.out.println(user);
		a.setPanId(user.getPanId());
		System.out.println(a);
		try {
			return new ResponseEntity<>(dao.updateAddress(a),HttpStatus.OK);
		}catch (RuntimeException e) {
			System.out.println("err in login " + e);
			return new ResponseEntity<>("address update failed : " + e.getMessage(), HttpStatus.OK);
		}
	}
	@PutMapping("/form1")
	public ResponseEntity<String> updateForm1(@RequestBody SalaryDetail s)
	{
		System.out.println("inside put form1");
		System.out.println(s);
		
		System.out.println(user);
		s.setPanId(user.getPanId());
		System.out.println(s);
		try {
			return new ResponseEntity<>(dao.updateSalaryDetails(s),HttpStatus.OK);
		}catch (RuntimeException e) {
			System.out.println("err in login " + e);
			return new ResponseEntity<>("address update failed : " + e.getMessage(), HttpStatus.OK);
		}
	}
	@PutMapping("/form2")
	public ResponseEntity<String> updateForm2(@RequestBody AdditionalIncome a)
	{
		System.out.println("inside put form2");
		System.out.println(a);
		
		System.out.println(user);
		a.setPanId(user.getPanId());
		System.out.println(a);
		try {
			return new ResponseEntity<>(dao.updateAdditionalIncome(a),HttpStatus.OK);
		}catch (RuntimeException e) {
			System.out.println("err in login " + e);
			return new ResponseEntity<>("Additional Income update failed : " + e.getMessage(), HttpStatus.OK);
		}
	}
	@PutMapping("/form3")
	public ResponseEntity<String> updateForm3(@RequestBody Deduction80C d)
	{
		System.out.println("inside put form3");
		System.out.println(d);
		
		System.out.println(user);
		d.setPanId(user.getPanId());
		System.out.println(d);
		try {
			return new ResponseEntity<>(dao.updateDeduction80C(d),HttpStatus.OK);
		}catch (RuntimeException e) {
			System.out.println("err in login " + e);
			return new ResponseEntity<>("Deduction 80C update failed : " + e.getMessage(), HttpStatus.OK);
		}
	}
	@PutMapping("/form4")
	public ResponseEntity<String> updateForm4(@RequestBody Deduction80D d)
	{
		System.out.println("inside put form4");
		System.out.println(d);
		
		System.out.println(user);
		d.setPanId(user.getPanId());
		System.out.println(d);
		try {
			return new ResponseEntity<>(dao.updateDeduction80D(d),HttpStatus.OK);
		}catch (RuntimeException e) {
			System.out.println("err in login " + e);
			return new ResponseEntity<>("Deduction 80D update failed : " + e.getMessage(), HttpStatus.OK);
		}
	}
	
	@GetMapping("/calculate")
	public ResponseEntity<?> calculateTax()
	{
		double sal=dao.getSalleryDetails(user.getPanId());
		double aIncome=dao.getAdditionalIncome(user.getPanId());
		double deduction80C=dao.getDeduction80C(user.getPanId());
		if(deduction80C>150000)
			deduction80C=150000;
		double deduction80D=dao.getDeduction80D(user.getPanId());
		if(deduction80D>35000)
			deduction80D=35000;
		double taxAbleAmount=sal+aIncome-deduction80C-deduction80D;
		System.out.println(taxAbleAmount);
		List<TaxSlab> slabs=dao.getTaxSlab();
		System.out.println(slabs);
		double taxAmount=0;
		for (TaxSlab taxSlab : slabs) {
			double temp=taxAbleAmount-taxSlab.getMaxLimit();
			if(temp>(taxSlab.getMaxLimit()-taxSlab.getMinLimit()))
				taxAmount=taxAmount+(temp*taxSlab.getRoi()/100);
			else if(temp<0)
			{
				taxAmount=taxAmount+((taxAbleAmount-taxSlab.getMinLimit())*taxSlab.getRoi()/100);
				break;
			}
			else
				taxAmount=taxAmount+((taxSlab.getMaxLimit()-taxSlab.getMinLimit())*taxSlab.getRoi()/100);
		}
		Address a=dao.getAddress(user.getPanId());
		TaxStatus tax=new TaxStatus();
		tax.setFirstName(user.getFirstName());
		tax.setPanId(user.getPanId());
		tax.setStatus("Under Approval");
		tax.setTaxableAmount(taxAbleAmount);
		tax.setTaxAmount(taxAmount);
		tax.setState(a.getState());
		dao.updateTaxStatus(tax);
		System.out.println(tax);
		return new ResponseEntity<>(tax,HttpStatus.OK) ;
	}
	@GetMapping("/taxstatus")
	public ResponseEntity<?> getTaxStatus()
	{
		return new ResponseEntity<TaxStatus> (dao.getTaxStatus(user.getPanId()),HttpStatus.OK);
	}
	@GetMapping("/deletestatus")
	public ResponseEntity<?> deleteTaxStatus()
	{
		return new ResponseEntity<String> (dao.deleteTaxStatus(user.getPanId()),HttpStatus.OK);
	}
	@GetMapping("/address")
	public ResponseEntity<?> getAddress()
	{
		return new ResponseEntity<Address> (dao.getAddress(user.getPanId()),HttpStatus.OK);
	}
	@GetMapping("/form1")
	public ResponseEntity<?> getForm1()
	{
		return new ResponseEntity<SalaryDetail> (dao.getForm1(user.getPanId()),HttpStatus.OK);
	}
	@GetMapping("/form2")
	public ResponseEntity<?> getForm2()
	{
		return new ResponseEntity<AdditionalIncome> (dao.getForm2(user.getPanId()),HttpStatus.OK);
	}
	@GetMapping("/form3")
	public ResponseEntity<?> getForm3()
	{
		return new ResponseEntity<Deduction80C> (dao.getForm3(user.getPanId()),HttpStatus.OK);
	}
	@GetMapping("/form4")
	public ResponseEntity<?> getForm4()
	{
		return new ResponseEntity<Deduction80D> (dao.getForm4(user.getPanId()),HttpStatus.OK);
	}
}

