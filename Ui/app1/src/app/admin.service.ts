import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})

export class AdminService {
  url = 'http://localhost:8080/Project/admin';
  isAdmin=false;
  constructor(private http:Http,private router:Router) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // check if user has logged in
    if (sessionStorage['admin_login_status'] == '1') {
      return true;
    }
    this.router.navigate(['/admin-login']);
    return false;
  }

  getSchemes(){
    return this.http.get(this.url+'/schemes');
  }
  getSlab(id:number){
    return this.http.get(this.url+'/'+id);
  }
  getReport(){
    return this.http.get(this.url+'/report');
  }
  getRevenue(){
    return this.http.get(this.url+'/revenue');
  }

  getStateReport(state :String){
    return this.http.get(this.url+'/state-report/'+ state);
  }
  getStates(){
    return this.http.get(this.url+'/states');
  }
  changeStatus(panId:string,status:string){
    const body={
      panId:panId,
      status:status
      
    }
    console.log(body)
    const headers = new Headers({'Content-Type': 'application/json'});
    const requestOptions = new RequestOptions({headers: headers});
    
    return this.http.post(this.url+'/changeStatus',body,requestOptions)
  }
  updateSlab(id:number,minLimit:number,maxLimit:number,roi:number){
    const body={
      id:id,

      minLimit:minLimit,
      maxLimit:maxLimit,
      roi:roi
      
    }
    const headers = new Headers({'Content-Type': 'application/json'});
    const requestOptions = new RequestOptions({headers: headers});
    return this.http.put(this.url + '/updateSlab', body,requestOptions);

  }
  taxSlab(){
    return this.http.get(this.url+'/taxSlab');
  }
  addScheme(name:string,description:string,category:string)
  {
    const body={
      name:name,
      description:description,
      category:category
      
    }
    const headers = new Headers({'Content-Type': 'application/json'});
    const requestOptions = new RequestOptions({headers: headers});
    return this.http.post(this.url + '/addScheme', body,requestOptions);
  }
  login(email: string, password: string) {
    const body={
      email:email,
      password:password
    }
    const headers = new Headers({'Content-Type': 'application/json'});
    const requestOptions = new RequestOptions({headers: headers});
    return this.http.post(this.url + '/login', body,requestOptions);
  }
}
