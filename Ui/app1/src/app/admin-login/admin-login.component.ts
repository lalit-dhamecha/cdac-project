import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  email='';
  password='';
  status='';
  constructor(private router:Router,private adminService:AdminService) { }
  onLogin()
  {
    this.adminService.login(this.email,this.password).subscribe(response=>{
      console.log(response);
      if(response["_body"]=='login successfull')
      {
        this.adminService.isAdmin=true;
        sessionStorage['admin_login_status'] = '1';
        this.router.navigate(['/admin-home']);
      }
      else
      {
        this.status="Invalid Credentials!!";
      }
    })

    
  }
  onCancel()
  {
    this.router.navigate(['/']);
  }

  ngOnInit() {
  }

}
