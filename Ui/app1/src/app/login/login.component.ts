import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { Session } from 'protractor';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email='';
  password='';
  status='';
  constructor(private router:Router,private userService:UserService) { }
  onLogin()
  {
    this.userService.login(this.email,this.password).subscribe(response=>{
      console.log(response);
      if(response["_body"]=='login successfull go to address'){
        sessionStorage['login_status'] = '1';
        this.router.navigate(['/address']);
      }
      else if(response["_body"]=='login successfull')
      {
        sessionStorage['login_status'] = '1';
        this.router.navigate(['/user-home']);
      }
      else
      {
        this.status="Invalid Credentials!!";
      }
    })

    
  }
  onCancel()
  {
    this.router.navigate(['/']);
  }
  ngOnInit() {
  }

}
