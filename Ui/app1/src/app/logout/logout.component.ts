import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private router:Router) {
    
   }

   onload(){
    
    sessionStorage['login_status'] = '0';
    sessionStorage['admin_login_status'] = '0';
    this.router.navigate(['/']);
   }
  ngOnInit() {
    this.onload()
  }

}
