import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-form2',
  templateUrl: './form2.component.html',
  styleUrls: ['./form2.component.css']
})
export class Form2Component implements OnInit {

  details={};
  houseRent:number;
  interestLoan:number;
  mutualFunds:number;
  iSavingAcc:number;
  iBonds:number;
  otherIncome:number;
  constructor(private router:Router,private userService:UserService) { 
    this.userService.getform2().subscribe(response=>{
      if(response.json()!=null){
      this.details=response.json();
      console.log(this.details)
      this.houseRent=this.details['houseRent'];
      this.interestLoan=this.details['interestLoan'];
      this.mutualFunds=this.details['mutualFunds'];
      this.iSavingAcc=this.details['iSavingAcc'];
      this.iBonds=this.details['iBonds'];
      this.otherIncome=this.details['otherIncome'];
      }
    })
  }
  onSubmit()
  {

    this.userService.updateForm2(this.houseRent,this.interestLoan,this.mutualFunds,this.iSavingAcc,this.iBonds,this.otherIncome).subscribe(response=>{
      if(response['_body']=='update successfull')
      {
        alert("Successfully Saved");
        this.router.navigate(['/form3']);
      }
      else
      alert("failed to update")
    })
  }
  onCancel()
  {
    this.router.navigate(['/user-home']);
  }
  ngOnInit() {
  }

}
