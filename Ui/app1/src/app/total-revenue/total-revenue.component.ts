import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-total-revenue',
  templateUrl: './total-revenue.component.html',
  styleUrls: ['./total-revenue.component.css']
})
export class TotalRevenueComponent implements OnInit {

  reports=[];
  totalRevenue=0;
  constructor(private router:Router,private adminService:AdminService) { }

  onLoad(){
    this.adminService.getRevenue().subscribe(response=>{
      this.reports=response.json();
      console.log(this.reports);
      this.reports.forEach(element => {
        this.totalRevenue=this.totalRevenue+element.taxAmount;
      });
    })
  }
  ngOnInit() {
    this.onLoad();
  }

}
