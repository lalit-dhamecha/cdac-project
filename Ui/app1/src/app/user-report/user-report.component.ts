import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-report',
  templateUrl: './user-report.component.html',
  styleUrls: ['./user-report.component.css']
})
export class UserReportComponent implements OnInit {

  reports={};
  constructor(private router:Router,private userService:UserService) {
    console.log('user report')
    this.userService.taxStatus().subscribe(response=>{
      this.reports=response.json();
    })

   }

   onEdit()
   {
    this.userService.deleteStatus().subscribe(response=>{
      console.log('deleted')
    })
      this.router.navigate(['/form1']);
   }
   onDelete()
   {
     this.userService.deleteStatus().subscribe(response=>{
       console.log('deleted')
       this.router.navigate(['/user-home']);
     })
   }
  ngOnInit() {
  }

}
