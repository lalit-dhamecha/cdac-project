import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSchemesComponent } from './add-schemes.component';

describe('AddSchemesComponent', () => {
  let component: AddSchemesComponent;
  let fixture: ComponentFixture<AddSchemesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSchemesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSchemesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
