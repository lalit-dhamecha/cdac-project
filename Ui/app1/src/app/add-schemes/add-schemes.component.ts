import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-add-schemes',
  templateUrl: './add-schemes.component.html',
  styleUrls: ['./add-schemes.component.css']
})
export class AddSchemesComponent implements OnInit {


  name='';
  description='';
  category='Category';
  constructor(private router:Router,private adminService:AdminService) { }

  onSubmit()
  {
    console.log(this.name)
    console.log(this.category)
    console.log(this.description)
    this.adminService.addScheme(this.name,this.description,this.category).subscribe(response=>{
      if(response['_body']="Scheme added successfully"){
        alert('added successfully')
        this.name='';
        this.description='';
        this.category='';
      this.router.navigate(['/add-schemes'])}
      else
      alert('something went wrong')
    })
  }
  onCancel()
  {
    this.router.navigate(['/admin-home'])
  }
  ngOnInit() {
  }

}
