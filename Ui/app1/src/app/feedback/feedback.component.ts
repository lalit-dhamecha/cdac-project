import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {

  
  firstName:string;
  email:string;
  description:string;
  constructor(private router:Router) { }

  onSubmit(){
    alert('Feedback submitted successfully ');
    this.router.navigate(['/home']);
  }

  onReset(){
    this.firstName='';
    this.email='';
    this.description=''
  }
  ngOnInit() {
  }

}
