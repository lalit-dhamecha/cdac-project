import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-tax-slab',
  templateUrl: './tax-slab.component.html',
  styleUrls: ['./tax-slab.component.css']
})
export class TaxSlabComponent implements OnInit {

  slabs=[];
  minLimit:0;maxLimit:0;roi:0;
  constructor(private router:Router,private adminService:AdminService) { 
    this.onLoad();
  }
  onLoad(){
    this.adminService.taxSlab().subscribe(response=>{
      this.slabs=response.json();
      console.log(this.slabs);
      
    })
  }

  onUpdate(slab){
    this.router.navigate(['/slab-detail'],{queryParams:{id:slab.id}});
  }
  onCancel(){
    this.router.navigate(['/admin-home'])
  }
  ngOnInit() {
  }

}
