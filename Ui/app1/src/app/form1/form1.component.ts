import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-form1',
  templateUrl: './form1.component.html',
  styleUrls: ['./form1.component.css']
})
export class Form1Component implements OnInit {

  
  details={};
  basic:number=0;
  hra:number=0;
  lta:number=0;
  specialAllowance:number=0;
  constructor(private router:Router,private userService:UserService) {
    this.userService.getform1().subscribe(response=>{
      if(response.json()!=null){
      this.details=response.json();
      console.log(this.details)
      this.basic=this.details["basic"];
      this.hra=this.details["hra"];
      this.lta=this.details['lta'];
      this.specialAllowance=this.details['specialAllowance'];
      }
      
    })
   }
  onSubmit()
  {
    this.userService.updateForm1(this.basic,this.hra,this.lta,this.specialAllowance).subscribe(response=>{
      if(response['_body']=='update successfull')
      {
        alert("Successfully Saved");
        this.router.navigate(['/form2']);
      }
      else
      alert("failed to update")
    })
   
  }
  onCancel()
  {
    this.router.navigate(['/user-home']);
  }
  ngOnInit() {
  }

}
