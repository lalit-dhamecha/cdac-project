import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  reports=[];
  constructor(private router:Router,private adminService:AdminService) { 

    
  }

  onLoad(){
    this.adminService.getReport().subscribe(response=>{
      this.reports=response.json();
      console.log(this.reports);
    })
  }
  onApprove(report){
    this.adminService.changeStatus(report.panId,'Approved').subscribe(response=>{
      console.log(response)
      if(response['_body']=='update successfull')
      this.ngOnInit();
      else
      alert('something went wrong')
    })
  }
  onDecline(report){
    this.adminService.changeStatus(report.panId,'Decline').subscribe(response=>{
      console.log(response)
      if(response['_body']=='update successfull')
      this.ngOnInit();
      else
      alert('something went wrong')
    })
  }
  ngOnInit() {
    this.onLoad();
  }

}
