import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-schemes',
  templateUrl: './schemes.component.html',
  styleUrls: ['./schemes.component.css']
})
export class SchemesComponent implements OnInit {

  schemes=[];
  isAdmin;
  constructor(private router:Router,private adminService:AdminService) {
    this.isAdmin=this.adminService.isAdmin;
    this.onLoad();
   }

  onLoad(){
    this.adminService.getSchemes().subscribe(response=>{
      this.schemes=response.json();
      console.log(this.schemes)
    })
  }

  onCancel(){
    this.router.navigate(['/admin-home'])
  }
  ngOnInit() {
  }

}
