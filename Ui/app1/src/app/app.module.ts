import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms'
import {RouterModule} from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { HelpComponent } from './help/help.component';
import { RegisterFormComponent } from './register-form/register-form.component';
import { AddressComponent } from './address/address.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { LoginComponent } from './login/login.component';
import { UserHomeComponent } from './user-home/user-home.component';
import { LinksComponent } from './links/links.component';
import { Form1Component } from './form1/form1.component';
import { Form3Component } from './form3/form3.component';
import { Form4Component } from './form4/form4.component';
import { Form2Component } from './form2/form2.component';
import { UserService } from './user.service';
import { TaxCalculationComponent } from './tax-calculation/tax-calculation.component';
import { LogoutComponent } from './logout/logout.component';
import { UserReportComponent } from './user-report/user-report.component';
import { SchemesComponent } from './schemes/schemes.component';
//import { NgbdCollapseBasicComponent } from './ngbd-collapse-basic/ngbd-collapse-basic.component';
import { AddSchemesComponent } from './add-schemes/add-schemes.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AdminService } from './admin.service';
import { TaxSlabComponent } from './tax-slab/tax-slab.component';
import { SlabDetailComponent } from './slab-detail/slab-detail.component';
import { ReportComponent } from './report/report.component';
import { StatisticComponent } from './statistic/statistic.component';
import { TotalRevenueComponent } from './total-revenue/total-revenue.component';
import { StateReportComponent } from './state-report/state-report.component';
//import {Ng2CarouselamosModule} from 'ng2-carouselamos';
//import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactUsComponent,
    FeedbackComponent,
    AboutUsComponent,
    HelpComponent,
    RegisterFormComponent,
    AddressComponent,
    ConfirmationComponent,
    LoginComponent,
    UserHomeComponent,
    LinksComponent,
    Form1Component,
    Form2Component,
    Form3Component,
    Form4Component,
    TaxCalculationComponent,
    LogoutComponent,
    UserReportComponent,
    SchemesComponent,
    AddSchemesComponent,
    AdminLoginComponent,
    AdminHomeComponent,
    TaxSlabComponent,
    SlabDetailComponent,
    ReportComponent,
    StatisticComponent,
    TotalRevenueComponent,
    StateReportComponent
  ],
  imports: [
  //NgbModule,
    //Ng2CarouselamosModule,
  BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot([
      {path:'',component:HomeComponent},
      {path:'contact-us',component:ContactUsComponent},
      {path:'feedback',component:FeedbackComponent},
      {path:'about-us',component:AboutUsComponent},
      {path:'help',component:HelpComponent},
      {path:'register-form',component:RegisterFormComponent},
      {path:'address',component:AddressComponent,canActivate: [UserService]},
      {path:'confirmation',component:ConfirmationComponent},
      {path:'login',component:LoginComponent},
      {path:'user-home',component:UserHomeComponent,canActivate: [UserService]},
      {path:'links',component:LinksComponent,canActivate: [UserService]},
      {path:'form1',component:Form1Component,canActivate: [UserService]},
      {path:'form2',component:Form2Component,canActivate: [UserService]},
      {path:'form3',component:Form3Component,canActivate: [UserService]},
      {path:'form4',component:Form4Component,canActivate: [UserService]},
      {path:'tax-calculation' ,component:TaxCalculationComponent,canActivate:[UserService]},
      {path:'logout' ,component:LogoutComponent},
      {path:'user-report' ,component:UserReportComponent,canActivate:[UserService]},
      {path:'schemes' ,component:SchemesComponent},
      {path:'add-schemes' ,component:AddSchemesComponent,canActivate: [AdminService]},
      {path:'admin-login' ,component:AdminLoginComponent},
      {path:'admin-home' ,component:AdminHomeComponent,canActivate: [AdminService]},
      {path:'tax-slab' ,component:TaxSlabComponent,canActivate: [AdminService]},
      {path:'slab-detail' ,component:SlabDetailComponent,canActivate: [AdminService]},
      {path:'report' ,component:ReportComponent,canActivate: [AdminService]},
      {path:'statistic' ,component:StatisticComponent,canActivate: [AdminService]},
      {path:'total-revenue' ,component:TotalRevenueComponent,canActivate: [AdminService]},
      {path:'state-report' ,component:StateReportComponent,canActivate: [AdminService]},
    ])

  ],
  providers: [UserService,AdminService],
  bootstrap: [AppComponent]
})
export class AppModule { }
