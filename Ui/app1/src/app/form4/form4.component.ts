import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-form4',
  templateUrl: './form4.component.html',
  styleUrls: ['./form4.component.css']
})
export class Form4Component implements OnInit {

  details={};
  medicalIns:number;
  iEducationalLoan:number;
  nHra:number;
  physicallyDisable:number;
  seniorCitizen:number;
  constructor(private router:Router,private userService:UserService) {
    this.userService.getform4().subscribe(response=>{
      if(response.json()!=null){
      this.details=response.json();
      console.log(this.details)
      this.medicalIns=this.details['medicalIns'];
      this.iEducationalLoan=this.details['iEducationalLoan'];
      this.nHra=this.details['nhra'];
      this.physicallyDisable=this.details['physicallyDisable'];
      this.seniorCitizen=this.details['seniorCitizen'];
      }
    })
   }
  onSubmit()
  {
    this.userService.updateForm4(this.medicalIns,this.iEducationalLoan,this.nHra,this.physicallyDisable,this.seniorCitizen).subscribe(response=>{
      if(response['_body']=='update successfull')
      {
        alert("Successfully Saved");
        this.router.navigate(['/tax-calculation']);
      }
      else
      alert("failed to update")
    })
    
  }
  onCancel()
  {
    this.router.navigate(['/user-home']);
  }
  ngOnInit() {
  }

}
