import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  url = 'http://localhost:8080/Project/user';
  constructor(private http:Http,private router:Router) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // check if user has logged in
    if (sessionStorage['login_status'] == '1') {
      return true;
    }

    this.router.navigate(['/login']);
    return false;
  }

  getAddress(){
    return this.http.get(this.url+'/address')
  }
  getform1()
  {
    return this.http.get(this.url+'/form1')
  }
  getform2()
  {
    return this.http.get(this.url+'/form2')
  }
  getform3()
  {
    return this.http.get(this.url+'/form3')
  }
  getform4()
  {
    return this.http.get(this.url+'/form4')
  }
  deleteStatus()
  {
    return this.http.get(this.url+'/deletestatus')
  }
  taxStatus(){
    return this.http.get(this.url+'/taxstatus')
  }
  taxCalculation()
  {
    return this.http.get(this.url+'/calculate')
  }
  signup(firstName: string, lastName: string, email: string, password: string,panId:string,contactNo:string,middleName:string,dob:Date) {
    const body = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password,
      panId:panId,
      contactNo:contactNo,
      middleName:middleName,
      dob:dob
    };
    console.log(body);
    const headers = new Headers({'Content-Type': 'application/json'});
    const requestOptions = new RequestOptions({headers: headers});

    return this.http.post(this.url + '/register', body, requestOptions);
  }
  updateForm4(medicalIns:number,iEducationalLoan:number,nHra:number,physicallyDisable:number,seniorCitizen:number)
  {
    const body = {
      medicalIns:medicalIns,
      iEducationalLoan:iEducationalLoan,
      nhra:nHra,
      physicallyDisable:physicallyDisable,
      seniorCitizen:seniorCitizen
     };
     console.log(body);
     const headers = new Headers({'Content-Type': 'application/json'});
     const requestOptions = new RequestOptions({headers: headers});
 
     return this.http.put(this.url + '/form4', body, requestOptions); 
  }
  updateForm3(epf:number,ppf:number,scss:number,nsc:number,taxSavingFD:number,taxSavingBonds:number,taxSavingFunds:number,
    nps:number,lic:number,pensionPlan:number,housingLoan:number,sunkanyaSamriddhi:number,tutionFees:number)
    {
      const body = {
        epf:epf,
        ppf:ppf,scss:scss,nsc:nsc,taxSavingFD:taxSavingFD,taxSavingBonds:taxSavingBonds,taxSavingFunds:taxSavingFunds,
    nps:nps,lic:lic,pensionPlan:pensionPlan,housingLoan:housingLoan,sukanyaSamriddhi:sunkanyaSamriddhi,tutionFees:tutionFees
       };
       console.log(body);
       const headers = new Headers({'Content-Type': 'application/json'});
       const requestOptions = new RequestOptions({headers: headers});
   
       return this.http.put(this.url + '/form3', body, requestOptions); 
    }
  updateForm2(houseRent:number,interestLoan:number,mutualFunds:number,iSavingAcc:number,iBonds:number,otherIncome:number)
  {
    const body = {
      houseRent:houseRent,
      interestLoan:interestLoan,
      mutualFunds:mutualFunds,
      iSavingAcc:iSavingAcc,
      iBonds:iBonds,
      otherIncome:otherIncome
     };
     console.log(body);
     const headers = new Headers({'Content-Type': 'application/json'});
     const requestOptions = new RequestOptions({headers: headers});
 
     return this.http.put(this.url + '/form2', body, requestOptions); 
  }
  updateForm1(basic:number,hra:number,lta:number,specialAllowance:number)
  {
    const body = {
      basic:basic,
      hra:hra,
      lta:lta,
      specialAllowance:specialAllowance
     };
     console.log(body);
     const headers = new Headers({'Content-Type': 'application/json'});
     const requestOptions = new RequestOptions({headers: headers});
 
     return this.http.put(this.url + '/form1', body, requestOptions); 
  }
  updateAddress(address1:string,address2:string,city:string,state:String,pincode:number)
  {
    const body = {
     address1:address1,
     address2:address2,
     city:city,
     state:state,
     pincode:pincode
    };
    console.log(body);
    const headers = new Headers({'Content-Type': 'application/json'});
    const requestOptions = new RequestOptions({headers: headers});

    return this.http.put(this.url + '/address', body, requestOptions); 
  }
  login(email: string, password: string) {
    const body = {
      
      email: email,
      password: password
    };
    console.log(body);
    const headers = new Headers({'Content-Type': 'application/json'});
    const requestOptions = new RequestOptions({headers: headers});

    return this.http.post(this.url + '/login', body);
  }
}
