import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-slab-detail',
  templateUrl: './slab-detail.component.html',
  styleUrls: ['./slab-detail.component.css']
})
export class SlabDetailComponent implements OnInit {

  slab={};
  maxLimit=0;
  minLimit=0;
  roi=0;
  id=0;
  constructor(private activatedRoute:ActivatedRoute,private router:Router,private adminService:AdminService) {
    this.activatedRoute.queryParams.subscribe(params=>{
      this.id=params['id'];
    this.adminService.getSlab(this.id).subscribe(response=>{
     this.slab=response.json();

    console.log(this.slab);
    this.maxLimit=this.slab['maxLimit'];
    this.minLimit=this.slab['minLimit'];
    this.roi=this.slab['roi'];
    })
    });

   }

  onUpdate(){
    this.adminService.updateSlab(this.id,this.minLimit,this.maxLimit,this.roi).subscribe(response=>{
      if(response['_body']=='update successfull')
      {
        alert("Successfully Saved");
        this.router.navigate(['/tax-slab']);
      }
      else
      alert("failed to update")
  
    })
  }
  onCancel(){
    this.router.navigate(['/tax-slab']);
  }
  ngOnInit() {
  }

}
