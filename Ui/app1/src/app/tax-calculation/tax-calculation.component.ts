import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-tax-calculation',
  templateUrl: './tax-calculation.component.html',
  styleUrls: ['./tax-calculation.component.css']
})
export class TaxCalculationComponent implements OnInit {

  details={};
  
  constructor(private router:Router,private userService:UserService) {
    this.userService.taxCalculation().subscribe(response=>{
      console.log(response);
      this.details=response.json();
      console.log(this.details);
    })
   }
   onCancel(){
    this.router.navigate(['/user-home']);
   }
   onSubmit()
   {
     alert("Submitted successfully");
     this.router.navigate(['/user-home']);
   }
  ngOnInit() {
  }

}
