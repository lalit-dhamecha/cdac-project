import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';


@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  panId='';
  firstName='';
  lastName='';
  middleName='';
  contactNo='';
  email='';
  dob:Date;
  password='';
  constructor(private router:Router,private userService:UserService) { }


  onSubmit()
  {
    if (this.panId.length == 0) {
      alert('Enter PAN ID');
    }
    else if (this.firstName.length == 0) {
      alert('Enter First Name');
    } else if (this.lastName.length == 0) {
      alert('enter last name');
    } else if (this.email.length == 0) {
      alert('enter email');
    } else if (this.password.length == 0) {
      alert('enter password');
    } else if (this.middleName.length == 0) {
      alert('enter middle name');
    }else if (this.contactNo.length == 0) {
      alert('enter contact no');
    } 
     else {
      console.log(this.dob);
      this.userService
        .signup(this.firstName, this.lastName, this.email, this.password,this.panId,this.contactNo,this.middleName,this.dob)
        .subscribe(response => {
          console.log(response);
          //const body = response.json();
          if (response['status'] == 201) {
            
            this.router.navigate(['/confirmation']);
          } else {
            alert('error while registering a user');
          }
        });
    }
  
  }
  onCancel()
  {
    this.router.navigate(['/']);
  }
  ngOnInit() {
  }

}
