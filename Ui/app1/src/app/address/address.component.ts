import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserHomeComponent } from '../user-home/user-home.component';
import { UserService } from '../user.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {


  details={};
  address1='';
  address2='';
  city='';
  state='';
  pincode=0;
  constructor(private router:Router,private userService:UserService) {
    this.userService.getAddress().subscribe(response=>{
      if(response.json()!=null){
      this.details=response.json();
      console.log(this.details)
      this.address1=this.details['address1']
      this.address2=this.details['address2']
      this.city=this.details['city']
      this.state=this.details['state']
      this.pincode=this.details['pincode']
      }
    })
   }
  onSubmit()
  {
    this.userService.updateAddress(this.address1,this.address2,this.city,this.state,this.pincode).subscribe(response=>{
      console.log(response);
      if(response['_body']=="update successfull")
      {
        this.router.navigate(['/user-home']);

      }
      
      else
      alert("failed to update")
    })
    
  }
  onCancel()
  {
    this.router.navigate(['/user-home']);
  }
  ngOnInit() {
  }

}
