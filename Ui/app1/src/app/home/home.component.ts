import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router:Router) {
    sessionStorage['login_status'] = '0';
   }

  images = ['../src/images/symbol.png','../src/images/logo.jpg','../src/images/symbol.png'];
  register()
  {
    this.router.navigate(['/register-form'])
  }
  login()
  {
    this.router.navigate(['/login'])
  }
  ngOnInit() {
  }

}
