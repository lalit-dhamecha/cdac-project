import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../admin.service';
import { $ } from 'protractor';

@Component({
  selector: 'app-state-report',
  templateUrl: './state-report.component.html',
  styleUrls: ['./state-report.component.css']
})
export class StateReportComponent implements OnInit {

  states=[];
  state='Select State';
  isStatus=false;
  reports=[];
  constructor(private router:Router,private adminService:AdminService) {
    this.adminService.getStates().subscribe(response=>{
      console.log(response)
      this.states=response.json();
    })
    // this.states.push('maharashtra')
    // this.states.push('delhi')
    // this.states.push('gujrat')
  }

  onSearch(){
    console.log(this.state);
    this.adminService.getStateReport(this.state).subscribe(response=>{
      console.log(response)
      this.isStatus=true
      this.reports=response.json();
    })
    
  }
  onLoad(){
    this.adminService.getRevenue().subscribe(response=>{
      this.states=response.json();
      console.log(this.states);

    })
  }
  ngOnInit() {
    //this.onLoad();
  }

}
