import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-form3',
  templateUrl: './form3.component.html',
  styleUrls: ['./form3.component.css']
})
export class Form3Component implements OnInit {

  details={};
  epf:number;
  ppf:number;
  scss:number;
  nsc:number;
  taxSavingFD:number;
  taxSavingBonds:number;
  taxSavingFunds:number;
  nps:number;
  lic:number;
  pensionPlan:number;
  housingLoan:number;
  sunkanyaSamriddhi:number;
  tutionFees:number;
  constructor(private router:Router,private userService:UserService) {
    this.userService.getform3().subscribe(response=>{
      if(response.json()!=null){
      this.details=response.json();
      console.log(this.details)
      this.epf=this.details['epf'];
      this.ppf=this.details['ppf'];
      this.scss=this.details['scss'];
      this.nsc=this.details['nsc'];
      this.taxSavingFD=this.details['taxSavingFD'];
      this.taxSavingBonds=this.details['taxSavingBonds'];
      this.taxSavingFunds=this.details['taxSavingFunds'];
      this.nps=this.details['nps'];
      this.lic=this.details['lic'];
      this.pensionPlan=this.details['pensionPlan'];
      this.housingLoan=this.details['housingLoan'];
      this.sunkanyaSamriddhi=this.details['sukanyaSamriddhi'];
      this.tutionFees=this.details['tutionFees'];
      }
    })
   }
  onSubmit()
  {
    this.userService.updateForm3(this.epf,this.ppf,this.scss,this.nsc,this.taxSavingFD,this.taxSavingBonds,this.taxSavingFunds,
      this.nps,this.lic,this.pensionPlan,this.housingLoan,this.sunkanyaSamriddhi,this.tutionFees).subscribe(response=>{
      if(response['_body']=='update successfull')
      {
        alert("Successfully Saved");
        this.router.navigate(['/form4']);
      }
      else
      alert("failed to update")
    })
  }
  onCancel()
  {
    this.router.navigate(['/user-home']);
  }
  ngOnInit() {
  }

}
